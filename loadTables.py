from accbball.models import State, Color, Team, Player
from queryFunc import *
import django
import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "myOrm.settings")

django.setup()


def loadState():
    file = open("inputData/state.txt")
    for line in file:
        state_id, name = line.split(' ')
        # State.objects.get_or_create(name = name[:-1]) # exclude \n
        add_state(name[:-1])
    file.close()


def loadColor():
    file = open("inputData/color.txt")
    for line in file:
        color_id, name = line.split(' ')
        # Color.objects.get_or_create(name = name[0:-1])
        add_color(name[:-1])
    file.close()


def loadTeam():
    file = open("inputData/team.txt")
    for line in file:
        team_id, name, state_id, color_id, wins, losses = line.split(' ')
        # Team.objects.get_or_create(name = name, state_id = State.objects.get(state_id = state_id), color_id = Color.objects.get(color_id = color_id), wins = wins, losses = losses)
        add_team(name, state_id, color_id, wins, losses)
    file.close()


def loadPlayer():
    file = open("inputData/player.txt")
    for line in file:
        player_id, team_id, uniform_num, first_name, last_name, mpg, ppg, rpg, apg, spg, bpg = line.split(
            ' ')
        # Player.objects.get_or_create(team_id = Team.objects.get(team_id = team_id), uniform_num = uniform_num, first_name = first_name, last_name = last_name, mpg = mpg, ppg = ppg, rpg = rpg, apg = apg, spg = spg, bpg =bpg)
        add_player(team_id, uniform_num, first_name,
                   last_name, mpg, ppg, rpg, apg, spg, bpg)
    file.close()


def main():
    loadState()
    loadColor()
    loadTeam()
    loadPlayer()


if __name__ == "__main__":
    main()
