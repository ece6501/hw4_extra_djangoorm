from django.apps import AppConfig


class AccbballConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'accbball'
