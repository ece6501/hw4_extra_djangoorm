import django
import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "myOrm.settings")
django.setup()

import loadTables
import excerciser

def main():
    loadTables.main()
    excerciser.main()


if __name__ == "__main__":
    main()