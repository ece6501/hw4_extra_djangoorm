from accbball.models import Player, Team, State, Color
import django
import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "myOrm.settings")
django.setup()


def add_player(team_id, uniform_num, first_name, last_name, mpg, ppg, rpg, apg, spg, bpg):
    Player.objects.get_or_create(team_id=Team.objects.get(team_id=team_id), uniform_num=uniform_num,
                                 first_name=first_name, last_name=last_name, mpg=mpg, ppg=ppg, rpg=rpg, apg=apg, spg=spg, bpg=bpg)


def add_color(name):
    Color.objects.get_or_create(name=name)


def add_state(name):
    State.objects.get_or_create(name=name)


def add_team(name, state_id, color_id, wins, losses):
    Team.objects.get_or_create(name=name, state_id=State.objects.get(
        state_id=state_id), color_id=Color.objects.get(color_id=color_id), wins=wins, losses=losses)


def query1(use_mpg,  min_mpg,  max_mpg, use_ppg,  min_ppg,  max_ppg, use_rpg,  min_rpg,  max_rpg, use_apg,  min_apg,  max_apg, use_spg,  min_spg,  max_spg, use_bpg,  min_bpg,  max_bpg):
    rslt = Player.objects.all()
    if use_mpg:
        rslt = rslt.filter(mpg__lte=max_mpg, mpg__gte=min_mpg)
    if use_ppg:
        rslt = rslt.filter(ppg__lte=max_ppg, ppg__gte=min_ppg)
    if use_rpg:
        rslt = rslt.filter(rpg__lte=max_rpg, rpg__gte=min_rpg)
    if use_apg:
        rslt = rslt.filter(apg__lte=max_apg, apg__gte=min_apg)
    if use_spg:
        rslt = rslt.filter(spg__lte=max_spg, spg__gte=min_spg)
    if use_bpg:
        rslt = rslt.filter(bpg__lte=max_bpg, bpg__gte=min_bpg)
    print("PLAYER_ID TEAM_ID UNIFORM_NUM FIRST_NAME LAST_NAME MPG PPG RPG APG SPG BPG")
    for player in rslt:
        print(player)


def query2(teamcolor):
    rslt = Team.objects.filter(color_id__name=teamcolor)
    print("NAME")
    for team in rslt:
        print(team.name)


def query3(team_name):
    rslt = Player.objects.filter(team_id__name=team_name).order_by(
        '-ppg').values_list("first_name", "last_name")
    print("FIRST_NAME LAST_NAME")
    for player in rslt:
        print(player[0], player[1])


def query4(team_state, team_color):
    rslt = Player.objects.filter(team_id__state_id__name=team_state, team_id__color_id__name=team_color).reverse(
    ).values_list("first_name", "last_name", "uniform_num")
    print("FIRST_NAME LAST_NAME UNIFORM_NUM")
    for player in rslt:
        print(player[0], player[1], player[2])


def query5(num_wins):
    rslt = Player.objects.filter(team_id__wins__gt=num_wins)
    print("FIRST_NAME LAST_NAME TEAM_NAME WINS")
    for player in rslt:
        print(player.first_name, player.last_name,
              player.team_id.name, player.team_id.wins)
