from queryFunc import *
import django
import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "myOrm.settings")
django.setup()


def main():
    query1(1, 35, 40, 0, 0, 0, 0, 5, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0)
    query1(0, 35, 40, 0, 0, 0, 1, 5, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0)
    query1(0, 35, 40, 0, 0, 0, 0, 5, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0)
    # query2("Orange")
    # query3("FloridaState")
    # query3("Duke")
    # query4("NC", "Red")
    # query4("NC", "DarkBlue")
    # query5(13)
    # add_player(1, 1, "yyyy", "xxxx", 1, 1, 1, 1, 1, 1)
    # query1(0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
    # query2("b")
    # query3("haha")
    # query4("NC", "Red")
    # query5(5)
    # add_player(1, 60, "xxxxx", "yyyy", 20, 20, 10, 10, 5.3, 5.3)
    # add_team("team_baldwin", 10, 3, 20, 0)
    # add_state("N/A")
    # add_color("perry-winkle")
    # query1(1, 10, 30, 0, 0, 40, 0, 0, 6, 0, 0, 5, 0, 0.0, 10.0, 0, 0.0, 10.0)
    # query2("Gold")
    # query3("FloridaState")
    # query4("FL", "Maroon")
    # query5(8)


if __name__ == "__main__":
    main()
